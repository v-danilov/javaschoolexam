package com.tsystems.javaschool.tasks.pyramid;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        try {
            //Sort collection
            List<Integer> intList = new ArrayList<>();
            intList.addAll(inputNumbers);
            Collections.sort(intList);

            //Preparing data agreagator to create array
            List<ArrayList<Integer>> agregator = new ArrayList<>();

            //counting total number of ints
            int total = intList.size();
            int cnt = 1;

            //Creating arraylist of arraylists
            for (int i = 0; i < total; ) {
                ArrayList<Integer> arrayList = new ArrayList<>();
                for (int j = 0; j < cnt; j++) {
                    arrayList.add(intList.remove(0));
                }
                i += cnt;
                cnt++;
                agregator.add(arrayList);
            }

            //Preparing number of rows for array
            int rowNumbers = agregator.get(agregator.size() - 1).size();
            //Preparing number of cols for array
            int columnNumbers = agregator.size();

            //Creating array
            int array[][] = new int[rowNumbers][2 * columnNumbers - 1];

            //Current index of arraylist
            int index = 0;

            //Middle index in array for start
            int middle = array[0].length / 2;

            //Fill the middle
            array[0][middle] = agregator.get(index++).get(0);


            //Fill body
            for (int i = 1; i < array.length; i++) {
                int c = 0;
                for (int j = middle - 1; j < array[i].length; j += 2) {
                    if(agregator.get(index).size() > c) {
                        array[i][j] = agregator.get(index).get(c++);
                    }
                }
                middle--;
                index++;
            }

            return array;

        }
        //Maybe better to pass parameters then catch OutOfMemory
        catch (OutOfMemoryError | Exception e){
            throw new CannotBuildPyramidException();
        }
    }
}
