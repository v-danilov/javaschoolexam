package com.tsystems.javaschool.tasks.calculator;

import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.script.ScriptEngineManager;
import javax.script.ScriptEngine;
import javax.script.ScriptException;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {

        if(statement == null || statement.isEmpty())
        {
            return null;
        }
        boolean isValid = checkBrackets(statement);
        isValid  = isValid && checkRegexp((statement));

        String result = null;
        if(isValid)
        {
            ScriptEngineManager mgr = new ScriptEngineManager();
            ScriptEngine engine = mgr.getEngineByName("JavaScript");
            try {
                result = engine.eval(statement).toString();
                if(!result.matches("[+-]?\\d*(\\.\\d+)?"))
                    result = null;
            } catch (ScriptException e ) {
                e.printStackTrace();
            }
        }

       return result;

    }

    private boolean checkBrackets(String str){
        if(str.indexOf('(') == -1 && str.indexOf(")") == -1){
            return true;
        }

            Stack<Character> stack = new Stack<Character>();
            for(int i = 0; i < str.length(); i++){
                char c = str.charAt(i);
                if(c == '(' ){
                    stack.push(c);
                }
                else if( c == ')'){
                    try{
                        char last = stack.pop();
                        if(last == '(' && c != ')' )
                            return false;
                    }
                    catch(Exception e){
                        return false;
                    }
                }
            }
            return stack.isEmpty();
    }

    private boolean checkRegexp(String str) {
        Pattern pattern = Pattern.compile("[\\\\.+\\/*-]{2,}|[,]");
        Matcher matcher = pattern.matcher(str);
        return !matcher.find();
    }

}


